<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
<title>TB Mega Menu</title>
<short_name>tb_megamenu</short_name>
<dc:creator>themebrain</dc:creator>
<type>project_module</type>
<api_version>8.x</api_version>
<supported_majors>1</supported_majors>
<default_major>1</default_major>
<project_status>published</project_status>
<link>https://www.drupal.org/project/tb_megamenu</link>
<terms><term><name>Projects</name><value>Modules</value></term><term><name>Maintenance status</name><value>Actively maintained</value></term><term><name>Development status</name><value>Under active development</value></term><term><name>Module categories</name><value>Administration</value></term><term><name>Module categories</name><value>Site Navigation</value></term><term><name>Module categories</name><value>Utility</value></term></terms>
<releases>
<release><name>tb_megamenu 8.x-1.x-dev</name><version>8.x-1.x-dev</version><tag>8.x-1.x</tag><version_major>1</version_major><version_extra>dev</version_extra><status>published</status><release_link>https://www.drupal.org/project/tb_megamenu/releases/8.x-1.x-dev</release_link><download_link>https://ftp.drupal.org/files/projects/tb_megamenu-8.x-1.x-dev.tar.gz</download_link><date>1512972185</date><mdhash>861fb268e45d6b1a85af1d552906c561</mdhash><filesize>76214</filesize><files><file><url>https://ftp.drupal.org/files/projects/tb_megamenu-8.x-1.x-dev.tar.gz</url><archive_type>tar.gz</archive_type><md5>861fb268e45d6b1a85af1d552906c561</md5><size>76214</size><filedate>1512972185</filedate></file><file><url>https://ftp.drupal.org/files/projects/tb_megamenu-8.x-1.x-dev.zip</url><archive_type>zip</archive_type><md5>ef856593eee72661a6054d98952aa8bf</md5><size>99099</size><filedate>1512972185</filedate></file></files><security>Project has not opted into security advisory coverage!</security><core_compatibility>8.x</core_compatibility></release></releases>
</project>
