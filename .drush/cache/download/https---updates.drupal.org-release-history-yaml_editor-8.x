<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
<title>YAML editor</title>
<short_name>yaml_editor</short_name>
<dc:creator>yannickoo</dc:creator>
<type>project_module</type>
<api_version>8.x</api_version>
<recommended_major>1</recommended_major>
<supported_majors>1</supported_majors>
<default_major>1</default_major>
<project_status>published</project_status>
<link>https://www.drupal.org/project/yaml_editor</link>
<terms><term><name>Projects</name><value>Modules</value></term><term><name>Maintenance status</name><value>Actively maintained</value></term><term><name>Development status</name><value>Under active development</value></term><term><name>Module categories</name><value>Utility</value></term></terms>
<releases>
<release><name>yaml_editor 8.x-1.0</name><version>8.x-1.0</version><tag>8.x-1.0</tag><version_major>1</version_major><version_patch>0</version_patch><status>published</status><release_link>https://www.drupal.org/project/yaml_editor/releases/8.x-1.0</release_link><download_link>https://ftp.drupal.org/files/projects/yaml_editor-8.x-1.0.tar.gz</download_link><date>1507935243</date><mdhash>367538736da5c398902f0512c67715ea</mdhash><filesize>9780</filesize><files><file><url>https://ftp.drupal.org/files/projects/yaml_editor-8.x-1.0.tar.gz</url><archive_type>tar.gz</archive_type><md5>367538736da5c398902f0512c67715ea</md5><size>9780</size><filedate>1507935243</filedate></file><file><url>https://ftp.drupal.org/files/projects/yaml_editor-8.x-1.0.zip</url><archive_type>zip</archive_type><md5>4e874649ffe4fd6bed8412822fd375dd</md5><size>13926</size><filedate>1507935243</filedate></file></files><security covered="1">Covered by Drupal's security advisory policy</security><core_compatibility>8.x</core_compatibility></release><release><name>yaml_editor 8.x-1.x-dev</name><version>8.x-1.x-dev</version><tag>8.x-1.x</tag><version_major>1</version_major><version_extra>dev</version_extra><status>published</status><release_link>https://www.drupal.org/project/yaml_editor/releases/8.x-1.x-dev</release_link><download_link>https://ftp.drupal.org/files/projects/yaml_editor-8.x-1.x-dev.tar.gz</download_link><date>1507930443</date><mdhash>a14e239f9d2182ca49b827396d0491d3</mdhash><filesize>9779</filesize><files><file><url>https://ftp.drupal.org/files/projects/yaml_editor-8.x-1.x-dev.tar.gz</url><archive_type>tar.gz</archive_type><md5>a14e239f9d2182ca49b827396d0491d3</md5><size>9779</size><filedate>1507930443</filedate></file><file><url>https://ftp.drupal.org/files/projects/yaml_editor-8.x-1.x-dev.zip</url><archive_type>zip</archive_type><md5>1c4349d33d8c976e3c22643d4d35fd94</md5><size>13929</size><filedate>1507930443</filedate></file></files><security>Dev releases are not covered by Drupal security advisories.</security><core_compatibility>8.x</core_compatibility></release></releases>
</project>
